package com.uala.movies.model;

import com.uala.movies.model.product.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {

    public static final String CELL_PHONE = "cell_phone";
    public static final String EMAIL = "cell_phone";
    public static final String SMS = "cell_phone";

    private List<Product> productListSeen = new ArrayList<>();
    private boolean sufficientBalance = false;
    private Map<String, String> communicationChannels = new HashMap<String, String>();

    public User(boolean sufficientBalance) {
        this.sufficientBalance = sufficientBalance;
    }

    public User() {
    }

    public Map<String, String> getCommunicationChannels() {
        return communicationChannels;
    }

    public void setCommunicationChannels(String type, String value) {
        this.communicationChannels.put(type, value);
    }

    public boolean removeCommunicationChannels(String type) {
        return this.communicationChannels.entrySet().removeIf(cc -> cc.getKey().equals(type));
    }

    public List<Product> getProductListSeen() {
        return productListSeen;
    }

    public void setProductListSeen(List<Product> productListSeen) {
        this.productListSeen = productListSeen;
    }

    public void setProductListSeen(Product product) {
        this.productListSeen.add(product);
    }

    public boolean isSufficientBalance() {
        return sufficientBalance;
    }

    public void setSufficientBalance(boolean sufficientBalance) {
        this.sufficientBalance = sufficientBalance;
    }
}
