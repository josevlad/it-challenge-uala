package com.uala.movies.model.product;

public class DocumentaryFilm extends Product {

    public DocumentaryFilm(String name, String releaseDate) {
        super(name, releaseDate);
    }
}
