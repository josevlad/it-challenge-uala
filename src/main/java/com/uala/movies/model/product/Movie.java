package com.uala.movies.model.product;

public class Movie extends Product {

    private Boolean isHasOscar = false;

    public Movie(String name, String releaseDate) {
        super(name, releaseDate);
    }

    public Movie(String name, String releaseDate, Boolean isHasOscar) {
        super(name, releaseDate);
        this.isHasOscar = isHasOscar;
    }

    public Boolean getHasOscar() {
        return isHasOscar;
    }

    public void setHasOscar(Boolean hasOscar) {
        isHasOscar = hasOscar;
    }
}
