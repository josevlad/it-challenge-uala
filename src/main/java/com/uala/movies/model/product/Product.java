package com.uala.movies.model.product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Product {

    private Long id;
    private String name;
    private Calendar releaseDate;

    public Product(String name, String releaseDate) {
        this.name = name;
        this.releaseDate = parseDate(releaseDate);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = parseDate(releaseDate);
    }

    public static Calendar parseDate(String date) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            cal.setTime(sdf.parse(date));
            return cal;
        } catch (ParseException e) {
            return null;
        }
    }

}
