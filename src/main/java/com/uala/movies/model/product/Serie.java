package com.uala.movies.model.product;

import com.uala.movies.model.product.Product;

public class Serie extends Product {

    private Integer seasons;

    public Serie(String name, String releaseDate) {
        super(name, releaseDate);
    }

    public Serie(String name, String releaseDate, Integer seasons) {
        super(name, releaseDate);
        this.seasons = seasons;
    }

    public Integer getSeasons() {
        return seasons;
    }

    public void setSeasons(Integer seasons) {
        this.seasons = seasons;
    }
}
