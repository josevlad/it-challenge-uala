package com.uala.movies.services;

import com.uala.movies.model.User;

import java.util.Map;

public class PaymentService {

    public boolean payment(User user) {
        if (!user.isSufficientBalance()) return false;

        // process to debit funds here
        return sendNotification(user.getCommunicationChannels());
    }

    private boolean sendNotification(Map<String, String> map) {
        return map.entrySet().stream().allMatch(cc ->
                cc.getKey().equals(User.CELL_PHONE) ||
                cc.getKey().equals(User.SMS) ||
                cc.getKey().equals(User.EMAIL)
        );
    }
}
