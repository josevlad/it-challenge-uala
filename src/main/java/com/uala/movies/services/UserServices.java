package com.uala.movies.services;

import com.uala.movies.model.product.DocumentaryFilm;
import com.uala.movies.model.product.Movie;
import com.uala.movies.model.product.Product;
import com.uala.movies.model.product.Serie;
import com.uala.movies.model.User;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Calendar.*;

public class UserServices {

    public boolean userIsOld(User user) {
        List<Product> collect = user.getProductListSeen().stream()
                .filter(product -> getDiffYears(product.getReleaseDate()) > 2)
                .collect(Collectors.toList());

        // 60% de productos viejos
        return (collect.size() * 100 / user.getProductListSeen().size()) > 60;
    }

    public boolean itIsInterestingProducts(User user) {
        List<Product> collect = user.getProductListSeen().stream()
                .filter(product -> validInterestingProduct(product))
                .collect(Collectors.toList());

        // 60% de productos viejos
        return (collect.size() * 100 / user.getProductListSeen().size()) > 60;
    }

    public static int getDiffYears(Calendar calendar) {
        Calendar today = getCalendar(new Date());
        int diff = today.get(YEAR) - calendar.get(YEAR);
        if (today.get(MONTH) > calendar.get(MONTH) ||
                (today.get(MONTH) == calendar.get(MONTH) && today.get(DATE) > calendar.get(DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public static boolean validInterestingProduct(Product product) {
        if (product instanceof Serie) {
            Serie serie = (Serie) product;
            return serie.getSeasons() == 4 || serie.getSeasons() == 5;
        }

        if (product instanceof Movie) {
            Movie movie = (Movie) product;
            return movie.getHasOscar();
        }

        if (product instanceof DocumentaryFilm) {
            DocumentaryFilm documentaryFilm = (DocumentaryFilm) product;
            List<String> words = Arrays.asList(documentaryFilm.getName().toLowerCase().split(" "));
            return words.contains("unofficial");
        }

        return false;
    }


}
