package com.uala.movies.services;

import com.uala.movies.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PaymentServiceTest {

    private PaymentService paymentService;

    @Before
    public void setUp() throws Exception {
        paymentService = new PaymentService();
    }

    /**
     * Efectuar cobro por parte de Uala-movies, donde debe realizar el cobro necesario y notificarle al usuario una vez
     * efectivizado. Cada usuario tiene créditos disponibles dentro del sistema. (caso 1)
     *
     */
    @Test
    public void shouldReturnTrueWhenProccesPayment() {
        //give
        User user = new User(true);
        user.setCommunicationChannels(User.CELL_PHONE, "123456789");

        //when
        boolean isProccesPayment = paymentService.payment(user);

        //then
        Assert.assertTrue("Oops! Procces Payment shoud be true", isProccesPayment);
    }



    /**
     * Efectuar cobro por parte de Uala-movies, donde debe realizar el cobro necesario y notificarle al usuario una vez
     * efectivizado. Cada usuario tiene créditos disponibles dentro del sistema. (caso 2)
     *
     */
    @Test
    public void shouldReturnFalseWhenProccesPayment() {
        //give
        User user = new User(false);
        user.setCommunicationChannels(User.CELL_PHONE, "123456789");

        //when
        boolean isProccesPayment = paymentService.payment(user);

        //then
        Assert.assertFalse("Oops! Procces Payment shoud be false", isProccesPayment);
    }

}