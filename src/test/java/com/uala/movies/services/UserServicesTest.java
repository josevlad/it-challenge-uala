package com.uala.movies.services;

import com.uala.movies.model.product.DocumentaryFilm;
import com.uala.movies.model.product.Movie;
import com.uala.movies.model.product.Product;
import com.uala.movies.model.product.Serie;
import com.uala.movies.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class UserServicesTest {

    private UserServices userServices;

    @Before
    public void setUp() throws Exception {
        userServices = new UserServices();
    }

    /**
     * Saber si un cliente es un antiguo. Lo es cuando solo mira productos viejos (caso 1)
     */
    @Test
    public void shouldReturnTrueWhenValidatingIfItIsAnOldUser() {
        //give
        User user = new User();
        List<Product> movies = Arrays.asList(
                new Movie("Titanic", "1997-01-01"),
                new Movie("Avatar", "2009-01-01"),
                new Movie("up", "2009-01-01"),
                new Movie("Dumbo", "2019-01-01")
        );
        user.setProductListSeen(movies);

        //when
        boolean isOld = userServices.userIsOld(user);

        //then
        Assert.assertTrue("Oops! user shoud be old", isOld);
    }

    /**
     * Saber si un cliente es un antiguo. Lo es cuando solo mira productos viejos (caso 2)
     */
    @Test
    public void shouldReturnFalseWhenValidatingIfItIsAnOldUser() {
        //give
        User user = new User();
        List<Product> movies = Arrays.asList(
                new Movie("Pantera negra", "2018-01-01"),
                new Movie("Deadpool 2", "2018-01-01"),
                new Movie("Titanic", "1997-01-01"),
                new Movie("Dumbo", "2019-01-01")
        );
        user.setProductListSeen(movies);

        //when
        boolean isOld = userServices.userIsOld(user);

        //then
        Assert.assertFalse("Oops! user shoud be new", isOld);
    }

    /**
     * Saber si un cliente mira algo interesante (caso 1)
     */
    @Test
    public void shouldReturnTrueWhenValidatingIfUserSeenInterestingContent() {
        //give
        User user = new User();
        List<Product> movies = Arrays.asList(
                new Movie("Pantera negra", "2018-01-01", true),
                new Serie("Deadpool 2", "2018-01-01", 4),
                new DocumentaryFilm("Titanic unofficial", "1997-01-01"),
                new Movie("Dumbo", "2019-01-01")
        );
        user.setProductListSeen(movies);

        //when
        boolean isInterestingProducts = userServices.itIsInterestingProducts(user);

        //then
        Assert.assertTrue("Oops! content shoud be Interesting", isInterestingProducts);
    }

    /**
     * Saber si un cliente mira algo interesante (caso 2)
     */
    @Test
    public void shouldReturnFalseWhenValidatingIfUserSeenInterestingContent() {
        //give
        User user = new User();
        List<Product> movies = Arrays.asList(
                new Movie("Pantera negra", "2018-01-01", false),
                new Serie("Deadpool 2", "2018-01-01", 2),
                new DocumentaryFilm("Titanic unofficial", "1997-01-01"),
                new Movie("Dumbo", "2019-01-01")
        );
        user.setProductListSeen(movies);

        //when
        boolean isInterestingProducts = userServices.itIsInterestingProducts(user);

        //then
        Assert.assertFalse("Oops! content shoud be NOT Interesting", isInterestingProducts);
    }

}